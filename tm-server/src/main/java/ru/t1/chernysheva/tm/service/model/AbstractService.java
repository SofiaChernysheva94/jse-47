package ru.t1.chernysheva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.service.IConnectionService;

public abstract class AbstractService {

    @NotNull
    protected IConnectionService connectionService;

}
