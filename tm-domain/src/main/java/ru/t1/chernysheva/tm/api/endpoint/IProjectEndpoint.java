package ru.t1.chernysheva.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.project.*;
import ru.t1.chernysheva.tm.dto.response.project.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndPoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static  IProjectEndpoint getInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIndexRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) throws Exception;

}
